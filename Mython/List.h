#ifndef LIST_H
#define LIST_H
#include "Sequence.h"
#include <list>
#include <string>

class List :public Sequence
{
	std::list<Type*> _list;
	char type;
public:
	List(std::list<Type*> list);
	//String(const String& other);
	virtual bool isPrintable() const;
	virtual std::string toString() const;
};

#endif // LIST_H