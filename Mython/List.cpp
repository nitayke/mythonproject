#include "List.h"

List::List(std::list<Type*> list)
{
	this->_list = list;
	this->type = 'l';
}

bool List::isPrintable() const
{
	return true;
}

std::string List::toString() const
{
	std::string output;
	output += "[";
	for (Type* i : this->_list)
	{
		output += i->toString() + ", ";
	}
	output = output.substr(0, output.size() - 2) + "]";
	return output;
}
